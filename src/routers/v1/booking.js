const express = require('express');
const bookingController = require('../../controllers/booking');

const bookingTicket = express.Router();

bookingTicket.post('/showtime', bookingController.createShowTime);
bookingTicket.post('/seat', bookingController.createSeat);

bookingTicket.put('/ticket', bookingController.bookingTicket);

bookingTicket.get('/cinemas', bookingController.getCinemasByShowTime);
bookingTicket.get('/', bookingController.getShowTimes);

module.exports = bookingTicket;
